package br.com.fujise.bollyfilmes.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by dan on 19/05/17.
 */

public class FilmesDBHelper extends SQLiteOpenHelper{

    private static final int DATABASE_VERSION =3;
    private static final String DATABASE_NAME = "bollyfilmes.db";


    public FilmesDBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        String sqlTablesFilmes = "CREATE TABLE " + FilmesContract.FilmeEntry.TABLE_NAME + " (" +
                FilmesContract.FilmeEntry._ID + " INTEGER PRIMARY KEY, " +
                FilmesContract.FilmeEntry.COLUMN_TITULO + " TEXT NOT NULL, " +
                FilmesContract.FilmeEntry.COLUMN_DESCRICAO + " TEXT NOT NULL, " +
                FilmesContract.FilmeEntry.COLUMN_POSTER_PATH + " TEXT NOT NULL, " +
                FilmesContract.FilmeEntry.COLUMN_CAPA_PATH + " TEXT NOT NULL, " +
                FilmesContract.FilmeEntry.COLUMN_AVALIACAO + " REAL, " +
                FilmesContract.FilmeEntry.COLUMN_DATA_LANCAMENTO + " TEXT NOT NULL, " +
                FilmesContract.FilmeEntry.COLUMN_POPULARIDADE + " REAL " +

                ");";
        db.execSQL(sqlTablesFilmes);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL("DROP TABLE " + FilmesContract.FilmeEntry.TABLE_NAME);
        onCreate(db);


    }
}
